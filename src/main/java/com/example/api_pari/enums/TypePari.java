package com.example.api_pari.enums;

public enum TypePari {
    VICTOIRE_DOMICILE,
    MATCH_NUL,
    VICTOIRE_EXTERIEUR
}
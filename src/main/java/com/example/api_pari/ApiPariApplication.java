package com.example.api_pari;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPariApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPariApplication.class, args);
	}

}


package com.example.api_pari.controllers;
import com.example.api_pari.models.UserPariModel;
import com.example.api_pari.services.PariService;
import com.example.api_pari.services.UserPariService;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userpari")
@CrossOrigin()
@RequiredArgsConstructor

public class UserPariController {

    @Autowired
    private final UserPariService userPariService;

    @Autowired
    private final PariService pariService;

    /**
     * Crée un nouveau pari utilisateur associé à un pari existant.
     *
     * @param existingPariId  L'ID du pari existant auquel associer le nouveau pari utilisateur.
     * @param userPari        Le modèle représentant le nouveau pari utilisateur.
     * @return ResponseEntity contenant le nouveau pari utilisateur créé avec le statut HTTP 201 (Created).
     */
    @PostMapping("/creer-bet/{existingPariId}")
    public ResponseEntity<UserPariModel> newPari(
            @PathVariable Long existingPariId,
            @RequestBody UserPariModel userPari) {
        UserPariModel nouveauPari = userPariService.createNewBet(userPari, existingPariId);
        return ResponseEntity.status(HttpStatus.CREATED).body(nouveauPari);
    }

    /**
     * Récupère tous les paris utilisateurs.
     *
     * @return ResponseEntity contenant la liste de tous les paris utilisateurs avec le statut HTTP 200 (OK).
     */
    @GetMapping("/get-all")
    public ResponseEntity<List<UserPariModel>> getAllUserParis() {
        List<UserPariModel> userParis = userPariService.getAllUserParis();
        return ResponseEntity.ok(userParis);
    }

    /**
     * Met à jour un pari utilisateur spécifié par son ID.
     *
     * @param id              L'ID du pari utilisateur à mettre à jour.
     * @param userPari        Les détails du pari utilisateur mis à jour.
     * @return ResponseEntity contenant le pari utilisateur mis à jour avec le statut HTTP 200 (OK).
     */
    @PutMapping("/update/{id}")
    public ResponseEntity<UserPariModel> updateUserPari(@PathVariable Long id, @RequestBody UserPariModel userPari) {
        UserPariModel updatedUserPari = userPariService.updateUserPari(id, userPari);
        return ResponseEntity.ok(updatedUserPari);
    }

    /**
     * Supprime un pari utilisateur spécifié par son ID.
     *
     * @param id              L'ID du pari utilisateur à supprimer.
     * @return ResponseEntity avec le statut HTTP 204 (No Content) si la suppression est réussie.
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteUserPari(@PathVariable Long id) {
        userPariService.deleteUserPariById(id);
        return ResponseEntity.noContent().build();
    }

}

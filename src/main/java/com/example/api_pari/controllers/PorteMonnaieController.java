package com.example.api_pari.controllers;

import com.example.api_pari.models.PorteMonnaieModel;
import com.example.api_pari.models.UserPariModel;
import com.example.api_pari.services.PorteMonnaieService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/portemonnaie")
@CrossOrigin()
@RequiredArgsConstructor

public class PorteMonnaieController {

    @Autowired
    private final PorteMonnaieService porteMonnaieService;

    /**
     * AJOUTER UN PARI A UN PORTE MONNAIE
     *
     * @param idPorteMonnaie L'identifiant du porte-monnaie auquel ajouter le pari.
     * @param idPari         L'identifiant du pari à ajouter.
     * @return ResponseEntity contenant le porte-monnaie mis à jour avec le pari ajouté.
     *         Retourne une réponse OK (200) si l'opération est réussie, une réponse
     *         NOT_FOUND (404) si le porte-monnaie ou le pari n'est pas trouvé, ou une
     *         réponse INTERNAL_SERVER_ERROR (500) en cas d'erreur interne.
     */
    @PostMapping("/{idPorteMonnaie}/addUser-pari/{idPari}")
    public ResponseEntity<PorteMonnaieModel> addPariToPorteMonnaie(
            @PathVariable Long idPorteMonnaie,
            @PathVariable Long idPari) {
        try {
            // Appeler la méthode du service pour ajouter le pari au porte-monnaie
            PorteMonnaieModel porteMonnaie = porteMonnaieService.addPariToPorteMonnaie(idPorteMonnaie, idPari);
            return ResponseEntity.ok(porteMonnaie);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Édite un pari dans un porte-monnaie spécifié.
     *
     * @param idPorteMonnaie L'identifiant du porte-monnaie contenant le pari à éditer.
     * @param idPari         L'identifiant du pari à éditer.
     * @param editedPari     Les nouvelles informations du pari à mettre à jour.
     * @return ResponseEntity indiquant le succès de l'opération.
     *         Retourne une réponse OK (200) si l'opération est réussie, ou une réponse
     *         NOT_FOUND (404) si le porte-monnaie ou le pari n'est pas trouvé, une
     *         réponse INTERNAL_SERVER_ERROR (500) en cas d'erreur interne.
     */
    @PutMapping("/{idPorteMonnaie}/edit-pari/{idPari}")
    public ResponseEntity<Void> editPariInPorteMonnaie(
            @PathVariable Long idPorteMonnaie,
            @PathVariable Long idPari,
            @RequestBody UserPariModel editedPari) {
        try {
            porteMonnaieService.editPariInPorteMonnaie(idPorteMonnaie, idPari, editedPari);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Supprime un pari d'un porte-monnaie spécifié.
     *
     * @param idPorteMonnaie L'identifiant du porte-monnaie contenant le pari à supprimer.
     * @param idPari         L'identifiant du pari à supprimer.
     * @return ResponseEntity indiquant le succès de l'opération.
     *         Retourne une réponse OK (200) si l'opération est réussie, une réponse
     *         NOT_FOUND (404) si le porte-monnaie ou le pari n'est pas trouvé, ou une
     *         réponse INTERNAL_SERVER_ERROR (500) en cas d'erreur interne.
     */
    @DeleteMapping("/{idPorteMonnaie}/delete-pari/{idPari}")
    public ResponseEntity<Void> deletePariFromPorteMonnaie(
            @PathVariable Long idPorteMonnaie,
            @PathVariable Long idPari) {
        try {
            porteMonnaieService.deletePariFromPorteMonnaie(idPorteMonnaie, idPari);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Récupère tous les paris associés à un porte-monnaie spécifié par son identifiant.
     *
     * @param idPorteMonnaie L'identifiant du porte-monnaie pour lequel récupérer les paris.
     * @return ResponseEntity contenant la liste des paris associés au porte-monnaie spécifié.
     *         Retourne une réponse OK (200) avec la liste des paris si l'opération est réussie,
     *         ou une réponse NOT_FOUND (404) si le porte-monnaie n'est pas trouvé.
     */
    @GetMapping("/{idPorteMonnaie}/all-paris")
    public ResponseEntity<List<UserPariModel>> getAllParisByPorteMonnaieId(@PathVariable Long idPorteMonnaie) {
        List<UserPariModel> paris = porteMonnaieService.getAllParisByPorteMonnaieId(idPorteMonnaie);
        return ResponseEntity.ok(paris);
    }

    /** ----- CRUD ----- **/
    @PostMapping("/creer-portemonnaie")
    public ResponseEntity<PorteMonnaieModel> creerPorteMonnaie(@RequestBody PorteMonnaieModel porteMonnaie) {
        PorteMonnaieModel newPorteMonnaie = porteMonnaieService.creerPorteMonnaie(porteMonnaie);
        return ResponseEntity.status(HttpStatus.CREATED).body(newPorteMonnaie);
    }

    @GetMapping("/get-all")
    public ResponseEntity<List<PorteMonnaieModel>> getAllPorteMonnaie() {
        List<PorteMonnaieModel> porteMonnaies = porteMonnaieService.getAllPorteMonnaie();
        return ResponseEntity.ok(porteMonnaies);
    }

    @PutMapping("/update/{idPorteMonnaie}")
    public ResponseEntity<PorteMonnaieModel> updatePorteMonnaie(@PathVariable Long idPorteMonnaie, @RequestBody PorteMonnaieModel updatedPorteMonnaie) {
        PorteMonnaieModel updatedPorteMonnaieResult = porteMonnaieService.updatePorteMonnaie(idPorteMonnaie, updatedPorteMonnaie);
        return ResponseEntity.ok(updatedPorteMonnaieResult);
    }

    @DeleteMapping("/delete/{idPorteMonnaie}")
    public ResponseEntity<Void> deletePorteMonnaie(@PathVariable Long idPorteMonnaie) {
        porteMonnaieService.deletePorteMonnaie(idPorteMonnaie);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{idPorteMonnaie}")
    public PorteMonnaieModel getPorteMonnaieById(@PathVariable Long idPorteMonnaie) {
        return porteMonnaieService.getPorteMonnaieById(idPorteMonnaie);
    }
    /** ---------------- **/
}

package com.example.api_pari.controllers;

import com.example.api_pari.models.PariModel;
import com.example.api_pari.services.PariService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pari")
@CrossOrigin()
@RequiredArgsConstructor

public class PariController {

    @Autowired
    private final PariService pariService;

    /**
     * Crée un nouveau pari en utilisant les informations fournies dans le corps de la requête.
     *
     * @param pari Le pari à créer, fourni dans le corps de la requête au format JSON.
     * @return ResponseEntity contenant le pari créé. Retourne une réponse CREATED (201)
     *         avec le pari créé dans le corps de la réponse si l'opération est réussie.
     */
    @PostMapping("/new-pari")
    public PariModel createPari(@RequestBody PariModel pari) {
        // Envoyer le pari à la méthode createPari du service pour la validation finale et l'enregistrement
        return pariService.createPari(pari);
    }

    /**
     * Récupère tous les paris existants dans le système.
     *
     * @return ResponseEntity contenant la liste de tous les paris existants.
     *         Retourne une réponse OK (200) avec la liste des paris dans le corps de la réponse.
     */
    @GetMapping("/get-all")
    public ResponseEntity<Object> getAllParis() {
        List<PariModel> allParis = pariService.getAllParis();
        return ResponseEntity.ok(allParis);
    }

    /**
     * Met à jour les détails d'un pari existant.
     *
     * @param id           L'ID du pari à mettre à jour.
     * @param pariDetails  Les détails mis à jour du pari.
     * @return ResponseEntity contenant le pari mis à jour.
     *         Retourne une réponse OK (200) avec le pari mis à jour dans le corps de la réponse.
     */
    @PutMapping("/edit-bet/{id}")
    public ResponseEntity<PariModel> updatePari(@PathVariable Long id, @RequestBody PariModel pariDetails) {
        PariModel updatedPari = pariService.updatePari(id, pariDetails);
        return ResponseEntity.ok(updatedPari);
    }

    /**
     * Récupère un pari par son ID.
     *
     * @param id L'ID du pari à récupérer.
     * @return ResponseEntity contenant le pari récupéré.
     *         Retourne une réponse OK (200) avec le pari dans le corps de la réponse.
     */
    @GetMapping("/get-bet-by-id/{id}")
    public ResponseEntity<PariModel> getPariById(@PathVariable Long id) {
        PariModel pari = pariService.getPariById(id);
        if (pari != null) {
            return ResponseEntity.ok(pari);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Supprime un pari en fonction de son ID.
     *
     * @param id L'ID du pari à supprimer.
     * @return ResponseEntity indiquant le succès de la suppression.
     *         Retourne une réponse No Content (204) après la suppression réussie du pari.
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deletePari(@PathVariable Long id) {
        pariService.deletePariById(id);
        return ResponseEntity.noContent().build();
    }

}

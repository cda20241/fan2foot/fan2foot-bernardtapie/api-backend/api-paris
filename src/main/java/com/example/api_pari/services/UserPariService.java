package com.example.api_pari.services;

import com.example.api_pari.models.PariModel;
import com.example.api_pari.models.PorteMonnaieModel;
import com.example.api_pari.models.UserPariModel;
import com.example.api_pari.repository.PariRepository;
import com.example.api_pari.repository.PorteMonnaieRepository;
import com.example.api_pari.repository.UserPariRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserPariService {

    @Autowired
    private final UserPariRepository userPariRepo;

    @Autowired
    private final PariRepository pariRepo;

    @Autowired
    private final PorteMonnaieRepository porteMonnaieRepo;

    /**
     * Crée un nouveau pari pour un joueur en utilisant les informations du pari existant.
     *
     * @param userPari                  Le nouveau pari à créer pour le joueur.
     * @param existingPariId            L'identifiant du pari existant à associer au nouveau pari.
     * @return                          Le nouveau pari créé pour le joueur.
     * @throws EntityNotFoundException  Si le pari existant avec l'ID spécifié n'est pas trouvé.
     * @throws IllegalArgumentException Si le montant misé est nul ou inférieur ou égal à zéro.
     * @throws ResponseStatusException  Si le solde du joueur est insuffisant pour placer ce pari.
     */
    public UserPariModel createNewBet(UserPariModel userPari, Long existingPariId) {
        // Rechercher le pari existant par son ID s'il n'existe pas ont renvoi une erreur
        PariModel existingPari = pariRepo.findById(existingPariId)
                .orElseThrow(() -> new EntityNotFoundException("Pari non trouvé avec l'ID spécifié: " + existingPariId));

        // Vérifier que la mise du joueur n'est pas égal à 0
        if (userPari.getMontant_mise() == null || userPari.getMontant_mise() <= 0) {
            throw new IllegalArgumentException("Le montant misé doit être supérieur à zéro.");
        }

        // Vérifier que le solde du joueur est suffisant pour placer le pari
        PorteMonnaieModel porteMonnaie = porteMonnaieRepo.findById(userPari.getPorteMonnaie().getId())
                .orElseThrow(() -> new EntityNotFoundException("Porte-monnaie non trouvé avec l'ID spécifié."));

        Float soldeJoueur = porteMonnaie.getSolde();
        if (soldeJoueur < userPari.getMontant_mise()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Solde insuffisant pour placer ce pari.");
        }

        // Déduire la mise du joueur de sont solde, puis on enregistre le nouveau solde
        porteMonnaie.setSolde(soldeJoueur - userPari.getMontant_mise());
        porteMonnaieRepo.save(porteMonnaie);

        // Associer le pari existant avec le nouveau pari de l'utilisateur
        userPari.setPariDetail(existingPari);

        // Enregistrer le nouveau pari de l'utilisateur
        return userPariRepo.save(userPari);
    }

    /**
     * Récupère tous les paris des utilisateurs.
     *
     * @return La liste de tous les paris des utilisateurs.
     */
    public List<UserPariModel> getAllUserParis() {
        return userPariRepo.findAll();
    }

    /**
     * Met à jour un pari utilisateur existant.
     *
     * @param id       L'ID du pari utilisateur à mettre à jour.
     * @param userPari Les nouvelles informations du pari utilisateur.
     * @return Le pari utilisateur mis à jour.
     * @throws IllegalArgumentException Si aucun pari utilisateur n'est trouvé avec l'ID spécifié.
     */
    public UserPariModel updateUserPari(Long id, UserPariModel userPari) {
        UserPariModel existingUserPari = userPariRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("UserPari not found with ID: " + id));

        // Mettre à jour les champs du UserPari existant avec les nouvelles valeurs
        existingUserPari.setMontant_mise(userPari.getMontant_mise());
        existingUserPari.setTypePari(userPari.getTypePari());
        existingUserPari.setCote(userPari.getCote());

        // Enregistrer et retourner le UserPari mis à jour
        return userPariRepo.save(existingUserPari);
    }

    /**
     * Supprime un pari utilisateur par son ID.
     *
     * @param id L'ID du pari utilisateur à supprimer.
     * @throws IllegalArgumentException Si aucun pari utilisateur n'est trouvé avec l'ID spécifié.
     */
    public void deleteUserPariById(Long id) {
        userPariRepo.deleteById(id);
    }

}

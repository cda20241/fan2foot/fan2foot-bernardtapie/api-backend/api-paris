package com.example.api_pari.services;

import com.example.api_pari.models.PariModel;
import com.example.api_pari.models.PorteMonnaieModel;
import com.example.api_pari.models.UserPariModel;
import com.example.api_pari.repository.PariRepository;
import com.example.api_pari.repository.PorteMonnaieRepository;
import com.example.api_pari.repository.UserPariRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class PorteMonnaieService {

    /** INJECTION DU SERVICE PORTE REPOSITORY **/
    @Autowired
    private final PorteMonnaieRepository porteMonnaieRepo;

    @Autowired
    private final PariRepository pariRepo;

    @Autowired
    private final UserPariRepository userPariRepo;


    /**
     * AJOUTER UN PARI A UN PORTE MONNAIE
     *
     * @param idPorteMonnaie           L'identifiant du porte-monnaie auquel ajouté le pari.
     * @param idPari                   L'identifiant du pari à ajouter.
     * @return                         Le porte-monnaie mis à jour après l'ajout du pari.
     * @throws EntityNotFoundException Si le porte-monnaie ou le pari du joueur n'est pas trouvé avec l'ID spécifié.
     */
    public PorteMonnaieModel addPariToPorteMonnaie(Long idPorteMonnaie, Long idPari) {
        // Récupérer le porte-monnaie
        PorteMonnaieModel porteMonnaie = porteMonnaieRepo.findById(idPorteMonnaie)
                .orElseThrow(() -> new EntityNotFoundException("Porte-monnaie non trouvé avec l'ID spécifié: " + idPorteMonnaie));

        // Récupérer le pari du joueur
        UserPariModel userPari = userPariRepo.findById(idPari)
                .orElseThrow(() -> new EntityNotFoundException("Pari du joueur non trouvé avec l'ID spécifié: " + idPari));

        // Ajouter le pari à la liste des paris du porte-monnaie
        List<UserPariModel> userParis = porteMonnaie.getUserPari();
        userParis.add(userPari);
        porteMonnaie.setUserPari(userParis);

        // Déduire la mise du solde

        // Enregistrer les modifications du porte-monnaie
        return porteMonnaieRepo.save(porteMonnaie);
    }

    /**
     * EDITER LE PARI D'UN PORTE MONNAIE
     *
     * @param idPorteMonnaie           L'identifiant du porte-monnaie contenant le pari à éditer.
     * @param idPari                   L'identifiant du pari à éditer.
     * @param editedPari               Les nouvelles informations du pari à mettre à jour.
     * @throws EntityNotFoundException Si le porte-monnaie ou le pari du joueur n'est pas trouvé avec l'ID spécifié.
     */
    public void editPariInPorteMonnaie(Long idPorteMonnaie, Long idPari, UserPariModel editedPari) {
        PorteMonnaieModel porteMonnaie = porteMonnaieRepo.findById(idPorteMonnaie)
                .orElseThrow(() -> new EntityNotFoundException("Porte-monnaie non trouvé avec l'ID spécifié: " + idPorteMonnaie));

        // Recherchez le pari dans la liste des paris du porte-monnaie et mettez à jour ses informations
        List<UserPariModel> userParis = porteMonnaie.getUserPari();
        for (UserPariModel pari : userParis) {
            if (pari.getId().equals(idPari)) {
                // Mettre à jour les champs du pari
                pari.setMontant_mise(editedPari.getMontant_mise());
                pari.setTypePari(editedPari.getTypePari());
                pari.setCote(editedPari.getCote());
                // Autres champs à mettre à jour si nécessaire
                break;
            }
        }
        // Enregistrer les modifications
        porteMonnaie.setUserPari(userParis);
        porteMonnaieRepo.save(porteMonnaie);
    }

    /**
     * SUPRIMER LE PARI D'UN PORTE MONNAIE
     *
     * @param idPorteMonnaie           L'identifiant du porte-monnaie contenant le pari à supprimer.
     * @param idPari                   L'identifiant du pari à supprimer.
     * @throws EntityNotFoundException Si le porte-monnaie ou le pari du joueur n'est pas trouvé avec l'ID spécifié.
     */
    public void deletePariFromPorteMonnaie(Long idPorteMonnaie, Long idPari) {
        PorteMonnaieModel porteMonnaie = porteMonnaieRepo.findById(idPorteMonnaie)
                .orElseThrow(() -> new EntityNotFoundException("Porte-monnaie non trouvé avec l'ID spécifié: " + idPorteMonnaie));

        // Supprimer le pari de la liste des paris du porte-monnaie
        List<UserPariModel> userParis = porteMonnaie.getUserPari();
        userParis.removeIf(pari -> pari.getId().equals(idPari));

        // Enregistrer les modifications
        porteMonnaie.setUserPari(userParis);
        porteMonnaieRepo.save(porteMonnaie);
    }

    /**
     * VOIR TOUS LES PARIS ASSOCIER A UN PORTE MONNAIE
     *
     * @param idPorteMonnaie           L'identifiant du porte-monnaie pour lequel récupérer les paris.
     * @return                         Une liste des paris associés au porte-monnaie spécifié.
     * @throws EntityNotFoundException Si le porte-monnaie n'est pas trouvé avec l'ID spécifié.
     */
    public List<UserPariModel> getAllParisByPorteMonnaieId(Long idPorteMonnaie) {
        PorteMonnaieModel porteMonnaie = porteMonnaieRepo.findById(idPorteMonnaie)
                .orElseThrow(() -> new EntityNotFoundException("Porte-monnaie non trouvé avec l'ID spécifié: " + idPorteMonnaie));

        return porteMonnaie.getUserPari();
    }

    /** ----- CRUD ----- **/
    public PorteMonnaieModel creerPorteMonnaie(PorteMonnaieModel porteMonnaie) {
        return porteMonnaieRepo.save(porteMonnaie);
    }

    public List<PorteMonnaieModel> getAllPorteMonnaie() {
        return porteMonnaieRepo.findAll();
    }

    public PorteMonnaieModel getPorteMonnaieById(Long idPorteMonnaie) {
        Optional<PorteMonnaieModel> porteMonnaie = porteMonnaieRepo.findById(idPorteMonnaie);
        if (porteMonnaie.isPresent()) {
            return porteMonnaie.get();
        }
        throw new IllegalArgumentException("Porte-monnaie non trouvé avec l'ID spécifié: " + idPorteMonnaie);
    }

    public PorteMonnaieModel updatePorteMonnaie(Long idPorteMonnaie, PorteMonnaieModel updatedPorteMonnaie) {
        Optional<PorteMonnaieModel> existingPorteMonnaieOptional = porteMonnaieRepo.findById(idPorteMonnaie);
        if (existingPorteMonnaieOptional.isPresent()) {
            PorteMonnaieModel existingPorteMonnaie = existingPorteMonnaieOptional.get();
            existingPorteMonnaie.setSolde(updatedPorteMonnaie.getSolde());
            // Vous pouvez également mettre à jour d'autres champs en fonction de vos besoins

            return porteMonnaieRepo.save(existingPorteMonnaie);
        } else {
            throw new IllegalArgumentException("Porte-monnaie non trouvé avec l'ID spécifié: " + idPorteMonnaie);
        }
    }

    public void deletePorteMonnaie(Long idPorteMonnaie) {
        porteMonnaieRepo.deleteById(idPorteMonnaie);
    }



}

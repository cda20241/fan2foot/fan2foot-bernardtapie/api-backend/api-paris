package com.example.api_pari.services;


import com.example.api_pari.models.PariModel;
import com.example.api_pari.repository.PariRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PariService {

    @Autowired
    private final PariRepository pariRepo;

    /**
     * Crée un nouveau pari dans la BDD.
     *
     * @param                           pari Le pari a créé.
     * @return                          Le pari créé avec succès.
     * @throws IllegalArgumentException si les valeurs des cotes du pari sont vides, nulles ou négatives.
     */
    public PariModel createPari(@NotNull PariModel pari) {
        // Vérifier que les valeurs des cotes ne sont pas vide à zéro ou négative
        if (pari.getCote_domicile() <= 0 || pari.getCote_nul() <= 0 || pari.getCote_exterieur() <= 0) {
            throw new IllegalArgumentException("Les cotes du pari doivent être spécifiées et positives.");
        }
        // Enregistrer le nouveau pari dans la base de données
        return pariRepo.save(pari);
    }

    /**
     * Récupère tous les paris présents dans la BDD.
     *
     * @return Une liste de tous les paris disponibles.
     */
    public List<PariModel> getAllParis() {
        return pariRepo.findAll();
    }

    /**
     * Récupère un pari par son ID.
     *
     * @param id L'ID du pari à récupérer.
     * @return Le pari correspondant à l'ID donné, ou null s'il n'existe pas.
     */
    public PariModel getPariById(Long id) {
        Optional<PariModel> pariOptional = pariRepo.findById(id);
        return pariOptional.orElse(null);
    }

    /**
     * Met à jour les informations d'un pari existant dans la BDD.
     *
     * @param id           L'identifiant du pari à mettre à jour.
     * @param pariDetails  Les détails du pari à mettre à jour.
     * @return Le pari mis à jour.
     * @throws EntityNotFoundException Si le pari avec l'ID spécifié n'est pas trouvé.
     */
    public PariModel updatePari(Long id, @NotNull PariModel pariDetails) {
        PariModel existingPari = pariRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Pari not found with id: " + id));

        // Mettre à jour les champs du pari existant avec les nouvelles valeurs
        existingPari.setCote_domicile(pariDetails.getCote_domicile());
        existingPari.setCote_nul(pariDetails.getCote_nul());
        existingPari.setCote_exterieur(pariDetails.getCote_exterieur());

        return pariRepo.save(existingPari);
    }

    /**
     * Supprime un pari de la BDD en utilisant son identifiant.
     *
     * @param id L'identifiant du pari à supprimer.
     * @throws EntityNotFoundException Si le pari avec l'ID spécifié n'est pas trouvé.
     */
    public void deletePariById(Long id) {
        if (!pariRepo.existsById(id)) {
            throw new EntityNotFoundException("Pari non trouvé avec l'ID spécifié: " + id);
        }
        pariRepo.deleteById(id);
    }

}

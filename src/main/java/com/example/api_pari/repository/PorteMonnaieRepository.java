package com.example.api_pari.repository;

import com.example.api_pari.models.PorteMonnaieModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PorteMonnaieRepository extends JpaRepository<PorteMonnaieModel, Long> {
}

package com.example.api_pari.repository;

import com.example.api_pari.models.UserPariModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPariRepository extends JpaRepository<UserPariModel, Long>{
}





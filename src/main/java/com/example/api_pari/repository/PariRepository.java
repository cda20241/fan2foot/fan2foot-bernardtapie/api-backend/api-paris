package com.example.api_pari.repository;

import com.example.api_pari.models.PariModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PariRepository extends JpaRepository<PariModel, Long> {
}


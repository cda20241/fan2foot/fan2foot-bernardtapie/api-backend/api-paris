package com.example.api_pari.models;

import com.example.api_pari.enums.TypePari;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@Entity
@RequiredArgsConstructor
@Table(name = "user_pari")

public class UserPariModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "montant_mise")
    private Float montant_mise;

    @Column(name = "type_pari")
    private TypePari typePari;

    @Column(name = "cote")
    private Float cote;

    @ManyToOne
    @JoinColumn(name = "id_porte_monnaie")
    private PorteMonnaieModel porteMonnaie;

    @ManyToOne
    @JoinColumn(name = "id_pari")
    private PariModel pariDetail;

}

package com.example.api_pari.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Entity
@RequiredArgsConstructor
@Table(name = "all_pari")

public class PariModel {

    @Id
    @Column(name = "id", nullable = false, updatable = false)

    private Long id;

    @Column(name = "cote_domicile")
    private Float cote_domicile;

    @Column(name = "cote_nul")
    private Float cote_nul;

    @Column(name = "cote_exterieur")
    private Float cote_exterieur;

}


package com.example.api_pari.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Table(name = "porte_monnaie")

public class PorteMonnaieModel {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "solde")
    private Float solde;

    @OneToMany
    @JoinColumn(name = "id_userPari")
    private List<UserPariModel> userPari;

}

